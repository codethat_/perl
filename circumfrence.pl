#!/usr/bin/perl -w
print ("Enter the radius:\n");
chomp($radius = <STDIN>);
$pi = 3.141592654;
$circ = 2 * $pi * $radius;
if ($radius < 0) {
  $circ = 0;
}
print ("The cicumference of a circle with radius $radius is $circ\n");
